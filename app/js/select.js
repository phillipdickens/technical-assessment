/* HTTP REQUEST - Assigns request object and opens connection  */
var request = new XMLHttpRequest();
request.open('GET', './data/player-stats.json', true);

/* PLAYERS STATS DATA - sends request, receives and parses JSON  */
request.onload = function () {
    data = this.response;
    data = JSON.parse(data);
    if (request.status >= 200 && request.status < 400) {
    } else {
        console.log('error');
    }
statsArray(data);
}
request.send();

/* GLOBAL variables - data defined and other variables required to set state of player card on load  */
var data;
var dropdownOpen = false;
var firstLoad = true;
var i, x;
var selectColour = true;
var lastPlayer;

/* LOADING GIF - fades on window load */
window.onload = function(e) {
    document.getElementById("loading-gif").style.opacity = "0";
    setTimeout(function(){
        document.getElementById("loading-gif").style.display = "none";
    }, 600);
};

/* CANVAS ARROW  - initial state with navigation menu dropdown closed */
var canvas = document.getElementById('nav-arrow'),
    ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.moveTo(0, 4);
    ctx.lineTo(8, 12);
    ctx.lineTo(16, 4);
    ctx.stroke();

/* STATS ARRAY - defines 2D stats array to handle data  */
var stats = new Array(5);
for (i = 0; i < 5; i++) {
    stats[i]=new Array(9);
};

/* POPULATE STATS ARRAY - makes calculations for stats not available directly from json file and fill stats array with the data needed for display   */
function statsArray() {console.log(data);
    for (i = 0; i < 5; i++) {
        stats[i][0] = data.players[i].player.name.last; /* first name */
        stats[i][1] = data.players[i].player.name.first + ' ' + stats[i][0];
        var pos = data.players[i].player.info.position; /* position */
        if (pos=='D') {stats[i][2]="Defender"}
        else if (pos=='M') {stats[i][2]="Midfielder"}
        else {stats[i][2]="Forward"};
        stats[i][3] = data.players[i].stats[6].value; /* appearances */
        stats[i][4] = data.players[i].stats[0].value; /* goals */
        stats[i][5] = data.players[i].stats[5].value; /* asissts */
        var goalsPerMatch = stats[i][4]/stats[i][3]; /* goals per match */
        stats[i][6] = goalsPerMatch.toFixed(2);
        var forwardPasses = data.players[i].stats[4].value;
        var backwardPasses = data.players[i].stats[8].value;
        var minutes = data.players[i].stats[7].value;
        var passesPerMinute = (forwardPasses+backwardPasses)/minutes; /* passes per minute */
        stats[i][7] = passesPerMinute.toFixed(2);
        stats[i][8] = 'badge'+i; /* club badge */
        stats[i][9] = 'player'+i; /* player image */
    }
    /* stats array sorted alphabetically by last name before players are added to navigation dropdown */
    stats.sort();
    for (i = 0; i < 5; i++) {
         document.getElementById("player"+i).innerHTML = stats[i][1];
     };
    if (firstLoad) {playerStats(0);};
};

/* PLAYER STATS - Populates elements on index with stats os selected player from stats array.  */
function playerStats(selected) {
    selectBox(selected);
    var badge = document.getElementById("badge");
    var badgeWrapper = document.getElementById("club-badge-wrapper");
    var player = document.getElementById("player-img");
    var playerWrapper = document.getElementById("player-image-wrapper");
    var name = document.getElementById("name");
    var position = document.getElementById("position");
    var statistic = document.getElementById("statistic");
    closeDropdown();
    /* Only if new player is chosen */
    if (selected!=lastPlayer) {
        if (!firstLoad) {
            /* css classes added at intervals to create animation */
            setTimeout(function(){
                badgeWrapper.className = "badge-flip";
                playerWrapper.className = "replace-image";
            }, 500);
            setTimeout(function(){
                name.className = "name-off-on-animated";
                position.className = "position-off-on-animated";
                document.getElementById("appearances").style.opacity = "0";
                document.getElementById("goals").style.opacity = "0";
                document.getElementById("assists").style.opacity = "0";
                document.getElementById("gpm").style.opacity = "0";
                document.getElementById("ppm").style.opacity = "0";
            }, 500);
            /* stats added while opacity = 0 */
            setTimeout(function(){
                document.getElementById("appearances").innerHTML = stats[selected][3];
                document.getElementById("goals").innerHTML = stats[selected][4];
                document.getElementById("assists").innerHTML = stats[selected][5];
                document.getElementById("gpm").innerHTML = stats[selected][6];
                document.getElementById("ppm").innerHTML = stats[selected][7];
            }, 1500);
            /* name and position chaned while off screen = 0 */
            setTimeout(function(){
                document.getElementById("name").innerHTML = stats[selected][1];
                document.getElementById("position").innerHTML = stats[selected][2];
            }, 1200);
            /* clears all iages before adding new player and badge image */
            setTimeout(function(){
                for (i = 0; i < 5; i++) {
                    if (i!=selected) {
                        badge.className = "";
                        player.className = "";
                    };
                };
                badge.className = stats[selected][8];
                player.className = stats[selected][9];
            }, 1000);
            /* stats made visible one at a time */
            setTimeout(function(){
                document.getElementById("appearances").style.opacity = "1";
                setTimeout(function(){
                    document.getElementById("goals").style.opacity = "1";
                }, 250);
                setTimeout(function(){
                    document.getElementById("assists").style.opacity = "1";
                }, 500);
                setTimeout(function(){
                    document.getElementById("gpm").style.opacity = "1";
                }, 750);
                setTimeout(function(){
                    document.getElementById("ppm").style.opacity = "1";
                }, 1000);
            }, 2600);
            /* animation classes no longer being used are removed */
            setTimeout(function(){
                name.className = "";
                position.className = "";
                badgeWrapper.className = "";
                playerWrapper.className = "";
            }, 4000); /* no animation of first load */
        }else {
            document.getElementById("name").innerHTML = stats[selected][1];
            document.getElementById("position").innerHTML = stats[selected][2];
            document.getElementById("appearances").innerHTML = stats[selected][3];
            document.getElementById("goals").innerHTML = stats[selected][4];
            document.getElementById("assists").innerHTML = stats[selected][5];
            document.getElementById("gpm").innerHTML = stats[selected][6];
            document.getElementById("ppm").innerHTML = stats[selected][7];
        };
        lastPlayer = selected;
    };

};

/* SELECT BOX - opens navigation dropdown on click  */
function openDropdown() {
    if (!dropdownOpen) {
      var open = document.getElementById("dropdown");
      open.className += " dropdown-open";
            navArrow();
      dropdownOpen=true;
      selectColour=false;

    }
};

/* SELECT BOX - closes navigation dropwn  */
function closeDropdown() {
    if (dropdownOpen) {
      setTimeout(function(){
          var close = document.getElementById("dropdown");
          close.className = "";
          /* add opacity 1 class after first player image has loaded */
          if (firstLoad) {
              var player = document.getElementById("player-image-wrapper");
              player.className += " player-image-load";
          }
      }, 300);
      setTimeout(function(){
          for (i = 0; i < 5; i++) {
              document.getElementById("select" + i).className = "player-select";
          }
          selectColour = false;
      }, 1200);
      setTimeout(function(){
          navArrow();
      }, 1000);
      dropdownOpen=false;
      firstLoad=false;
    }
};


/* SELECT BOX - highlights selection as navigation dropdown closes  */
function selectBox(selected) {
    if (!selectColour) {
        document.getElementById("select" + selected).className += " selected";
        for (i = 0; i < 5; i++) {
            if (i!=selected ) {
                document.getElementById("select" + i).className += " not-selected";
            }
        }
    selectColour = false;
    };
};

/* SELECT BOX ARROW - Draws new arrow after navigation dropwdon opens and closes  */
function navArrow() {
    i = 0;
    x = 0;
    var draw = setInterval(frame, 50);
    function frame() {
        if (i == 9) {
          clearInterval(draw);
        } else {
            if (x<9) {
                x++;
            } else {
                i++;
            };
            if (dropdownOpen) {
              ctx.clearRect(0, 0, canvas.width, canvas.height);
              ctx.beginPath();
              ctx.moveTo(0, 4 + x);
              ctx.lineTo(8, 12 - i);
              ctx.lineTo(16, 4 + x);
              ctx.stroke();
            } else {
              ctx.clearRect(0, 0, canvas.width, canvas.height);
              ctx.beginPath();
              ctx.moveTo(0, 12 - x);
              ctx.lineTo(8, 4 + i);
              ctx.lineTo(16, 12 - x);
              ctx.stroke();
            }
        }
    }
};



